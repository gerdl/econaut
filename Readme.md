# Introduction

Basic assumption: Only a tiny subset of all possible economic systems was tested by humans so far - typically discussions around economic systems revolve around communism vs capitalism as if this was a one-dimensional axis. 

In an effort to an unbiased mapping of arbitrary economic systems, we are building a simplistic model of a production/consumption system, initially separating away the financial/economic System. Later different implementations of economic systems under consideration should be pluggable into the simulation for analysis.

Simulation Properties:
- Delocalised, 
- Discrete Timesteps
- Discrete Actors
- Discrete but open-ended number of Product Types
- Continuous Quantities of goods to be consumed

## Features

- **Technological Progress**: Instead of supplying a fixed list of possible product types, the list is random-generated and open ended, such that a simulated society could constantly develop new products to simulate advance in technology.
- **Complex Products**: Various kinds of products should be possible - from food over to cars, clothing, houses and up to services (modeled as different spoil-rates and consumption counters)
- **Abstract Products**: As it is virtually impossible to re-create a real-life system of products and their dependencies, we create an artificial network of products and their dependencies
- **Personal Preferences**: Simulated actors should have different abilities and preferences for the production and consumption of particular goods
  
## Not-yet-implemented but planned features

- **Co-operative production**: such that multiple actors can join in the creation of large products (e.g., space shuttle) - required for the simulation of large firms
- **Complex product composition**: Products cannot yet be composed of other other products + work
- No **useful economy** is implemented so far
- No **State Spending** yet: Currently there is no state that would buy roads, education, etc.

## Non-Features - or not yet in the plan:

The following features are out of the simulation scope.

- **personal life-cycle**: Actors do not age, do not reproduce, do not evolve, do not die if starving etc. Education might later be added.
- **No growth/shrinking of population**
- **No politics**: The political process of decisionmaking and of moving from one economic system to another is out of scope of this simulation
- **No resale** - simulation actors currently don't re-sell their used/ages stuff

# Archtitecture
## Components

A schematic overview of the components is shown in:
![Econaut Components](docs/uml/components.png "Econaut Components")


- Tech Tree
    - procedurally generated, open ended forest of dags:
    - which goods/services are required to produce new service/goods

- ProductType:
    - **consumption_per_use**:
        - Defines how many time this product can be used?
           - For services like a "haircut", this would be 1
           - For an Apple, this would be one
           - For Car this could for example be 1/10'000
           - Ideas, Computer Programs, etc.: 0 
    - **max_use_per_time**: how many times per iteration can an actor consume a product type, given it obtained enough of it and given the consumption still increases the actor's satisfaction.
    - TODO: list_of_requirements (How many/which products require which others)
        - For a "haircut" service, "scissors" might be required.
        - For a computer, a long/complex list of  parts might be required
        - Bread requires flour and yeast, etc.
    - **prod_manpower**:
        - How many "mandays" times scaling factor are required for production
    - TODO: production_discount:
        - some products become cheaper, when you 
          producte more of them - e.g., cars
    - TODO: list(replaces) -> this product replaces another product (probably with less manual effort)
        - e.g., a tractor replaces a plow
        - an electric oven replaces a coal-burning stove
        - TODO: Maybe look for a more elegant formulation here
    - **is_consumer_good**: 
        - industrial products like a printing press or a 12000 hp diesel motor vs. a car or an apple
    - **spoil_rate**:
        - i.e., "best-before...")
        - Fraction of goods that decay if not used immediately
        - use 1.0 for services: We can't produce a haircut in advance
    - **pref_hash**: See Implementation Details
    
    - **satisfaction**: consumer goods serve specific human needs like
      shelter, nutrition, warmth, medicine, clothing etc... 
      People like to consume a range from [c_min, c_max] of each category.
      If less than c_min of an essential good, they are "starving", if more 
      than c_max of any good is available, it is "wasted".
      - possiby, new needCategories are added like new levels of civilization, 
        e.g., "media consumption" might not have been a need for 10'000 bc civilization.  
      

- Product:
    - ptype -> ProductType
    - quantity: float fraction of the product that is still usable

- Actor:
    - work_per_day: scale factor - 1-200%
    - talented: 50-150%
    - education: 10-500%
    - consumption_pref_hash
    - production_pref_hash
    - possession:        
      - list of things that can be consumed
      - not same as property/ownership - that concept could/would be added by the economic system


## Simulation Process

A coarse schematic overview of the main simulation loop is given in:
![Main Simloop](docs/uml/main_simloop.png "Econaut Main Simloop")

- Actors can possess, produce and consume products.

- In the main simulation loop,
  - all actors consume possessed products to satisfy their needs
  - possessed products can deteriorate, i.e. spoil, determined by the ProductTypes's spoil rate
  - actors spend the sim step (i.e. workday) for creating products
    - production progress is determined by different factors -> see Implementation Details
  
- Via calls to the Economy-Interface, the economic system controls: 
  - How much effort an actor chooses for production?
  - How an actor selects what to produce?
  - Who gains the results of the production process?
  - Which products an actor can exchange with other actors?


#### TODO: How to simulate group-productions

# Implementation Details
## Production- / Consumption Preference Hashing

In the assumption that people are different, I assume that each Actor has a 
particular preference for the consumption and production of particular goods.
To define an arbitrary but deterministic mapping of Product to Preference for each actor,
I define an actor-specific preference_hash that I can XOR or hash with the product_hash.
The resulting (product x actor) hash value is divided by the maximum value of the hash to generate a 
float value between 0 and 1. (TODO: maybe use Gray code https://en.wikipedia.org/wiki/Gray_code for this mapping later.)

```
    ha = actor.production_preference_hash
    hp = product_type.pref_hash
    
    pref = (ha xor hp) / MAXVAL
```

## TODO: Need/Satisfier Vectors

Our assumption is that each actor has a number of NEED_DIMS different needs and each product use can satisfy a part of the actor's needs. 
If we had only a single need/satisfaction category instead, each actor could satisfy itself and wouldn't need trad or other people!

```
    Nvect = (( n_i )) for i in NEED_DIMS
    Svect = (( s_i ))
```

For simplicity, we will not distinguish between essential/non-essential needs for now and assume that we will try to maximize each category!


# TODO: Economic Systems:



## TODO: Non-Monetary Economies:

### TODO: Pioneers or Hunter/Gatherers:
- I can only consume what I produce myself

### TODO: Hard-Core Communism:
- Demand determines what is produced



## TODO: Money-Using Economies


### TODO: Feudalism

### TODO: Basic Market Capitalism

- add components:
    - money
    - property (something in the possession of sb. else could be my property)

- use money for res. alloc
- process each round:
    - run market:
        - have maps: demand_2_actor and supply_2_actor
        - seller determines price, buyer determines buy/no-buy
    
    - agent modifies strategy:
        - all sold: sell more expensive + increase effort
        - not all sold: sell cheaper + reduce effort
        - scan through (all?) possible production options:
            - Can I produce smth., that gives a higher
- seller determines 

#### TODO: Capitalism + Trading:
- buy/sell stuff at different locations
    -> split up regions


#### TODO: Corporate Capitalism

- Company (Actor):    // a real person or a company
  - money
  - possession: Products owned - not necessarily used
    - A person/company can possess buildings but rent it out to others



- FinancialProducts:
    - Credit

#### TODO: Soft Capitalism + Unconditional Basic Income



### TODO: Anarchy:
- No tax
- property subject to 




# Design Decisions

## Open Questions

- Is it really necessary to simulate a **complex web of products** and production processes?
  - I don't know... But it sounds trivial to regulate a super-simple production/simulation system that has only 5 kinds of resources.
  - In a simple case, a planned-economy might work very nicely while it would fail for more complex systems.
  - I think a good economic system should be stable and useful also for complex technologies

- Is it really necessary to **simulate different** essential/non-essential **need categories**?
  - I.e., why isn't it enough to rely on the different consumption preferences?
  - But how do we distinguish between starvation, sufficiency and luxury situations, then? We don't want somebody to be considered living a luxury life if all he/she's got are tomatoes...!
  - So we need need categories to require a mixture of products for survival/luxury.
  - **Solution**: Without the need for a complex product mix, a market-like system that allows exchanging goods would not be required.

- How does an actor decide what it **wants/tries to possess/buy**? Or is this part of the economy?
  - Option a) Starting from what we consumed today, we try to consume the same tomorrow + more/less dependent on leftovers!
  - Option b) We have a user-specific mapping productType2satisfaction -> 

- How can we model the **joint work of many people** that assemble into a **firm** to make... cars... for example.

- How can we simulate **technological advance** in the sense of one newer product replacing an old one?

- How do we model shared infrastructure - a road or a factory that is not owned by a single worker?

- How shall we **model motivation** to put effort into work? See https://gitlab.com/gerdl/econaut/-/wikis/modelling_motivation

## Current Solution

- How to model an actor's preference for the production and consumption of a particular product?
    - See implementation details / preference hashing!

# Literature

## Abstractions of economic systems

- Harari, Y.N.: "A Brief History of Humankind" -> Socioeconomic Systems are only a fiction created by human imagination