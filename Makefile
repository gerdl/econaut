PWD := $(shell pwd)
UID := $(shell id -u)
GID := $(shell id -g)

notebook: build
	docker run \
		   --user $(UID):$(GID) \
	       --mount type=bind,source="$(PWD)",target=/home/econaut/host \
	       -p 8888:8888 \
	       -it econaut_nb:latest

shell: build
	docker run \
		   --user $(UID):$(GID) \
	       --mount type=bind,source="$(PWD)",target=/home/econaut/host \
	       -p 8888:8888 \
	       -it econaut_nb:latest bash

build:
	docker build -t econaut_nb docker


clean:
	docker system prune
