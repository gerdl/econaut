import random

from econaut.config.econaut_config import conf, CF_HASH_BITS


_max_value = 2**conf[CF_HASH_BITS]


def get_rand_hash():
    """Abstract away the random hash pattern generation - we might want to exchange this generation later!"""
    return random.getrandbits(conf[CF_HASH_BITS])


def to_float(val: int) -> float:
    """Map a hash value to a float between 0 and 1"""
    #  TODO: Maybe rather use gray code for float conversion!
    return val / _max_value
