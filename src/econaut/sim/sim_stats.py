import pandas as pd

from econaut.model.product import Product
from econaut.model.product_type import ProductType

ST_PROD_TOTAL       = "prod_total"            # production per generation
ST_CONS_TOTAL       = "cons_total"            # consumption per generation
ST_POSS_TOTAL       = "poss_total"
ST_SATISFACTION_AVG = "satis_avg"


class SimStats:
    """
    Collect statistic on simulation runs.

    """
    def __init__(self):
        self.fields = (
            ST_PROD_TOTAL,
            ST_CONS_TOTAL,
            ST_POSS_TOTAL,
            ST_SATISFACTION_AVG,
        )

        self.state = {
            key: 0 for key in self.fields
        }

        self._time_series = pd.DataFrame(
            columns=self.fields
        )

    def register_production(self, products: [Product]):
        for prod in products:
            self.state[ST_PROD_TOTAL] += prod.quantity

    def register_consumption(self, product_type: ProductType, quantity: float, actor: 'Actor'):
        self.state[ST_CONS_TOTAL] += quantity

    def register_spoilage(self, product_type: ProductType, quantity: float, actor: 'Actor'):
        pass

    def finalize_generation(self, actors) -> None:
        """
        Wrap up the results of one generation in a new time-series entry and prepare next gen.

        :param actors:
            list of actors used to initialize this iteration
        """
        # collect data at end of iteration
        for actor in actors:

            # posessions:
            for ptype, prod in actor.possession_dict.items():
                self.state[ST_POSS_TOTAL] += prod.quantity

            # actor satisfaction:
            self.state[ST_SATISFACTION_AVG] += actor.needs.effective_satisfaction_score()

        self.state[ST_SATISFACTION_AVG] /= len(actors)

        # Add new time frame
        self._time_series = self._time_series.append(self.state, ignore_index=True)

        # reset data for next round:
        for f in self.fields:
            self.state[f] = 0

    @property
    def time_series(self):
        return self._time_series
