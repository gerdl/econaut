import logging
import random

from econaut.economy.abstract_economy import AbstractEconomy
from econaut.model.actor import Actor
from econaut.model.product import Product
from econaut.sim.sim_stats import SimStats


_logger = logging.getLogger(__name__)


class SimpleSim:

    def __init__(self, economy: AbstractEconomy, stats: SimStats):
        self.actors = [Actor() for _ in range(100)]
        self._economy = economy
        self._stats = stats

    def _sim_step(self):
        """
        Run a single simulation iteration:

        - each actor:
          - consumes consumable stuff in his possession based on his preferences
          - produces stuff

          - updates its production preference based on the economic system
          - acquires stuff (based on economy)
          - gives away stuff (based on economy)

        """
        self._economy.init_sim_step()

        # Consume and Produce Stuff
        for actor in self.actors:

            # actors have this one chance to consume stuff produced last iteration before spoil
            actor.consume_possessions(self._stats)

            # after consumption, poessessions get spoiled according to spoil rate
            actor.spoil_possessions(self._stats)

            # compute motivation and produce goods:
            if actor.target_product is not None:
                # TODO: Decide if we can leave the decision for motivation "in the hands" of the economy
                #       or if we can deduce the motivation from other factors...
                motivation = self._economy.get_production_motivation(actor, actor.target_product)
                assert motivation >= 0.0
                assert motivation <= 1.0
                n_produced = actor.produce(motivation)

                if n_produced > 0:
                    new_stuff = Product(actor.target_product, _quantity=n_produced)

                    self._economy.distribute_production(producer=actor,
                                                        products=[new_stuff])
                    self._stats.register_production([new_stuff])

        # exchange products with other actors
        self._economy.exchange_goods(self.actors)

        # Overthink current production target
        for actor in self.actors:
            # Look into economy to correct target product
            self._economy.update_actor_target_product(actor)

        # TODO: group collaboration production

        self._stats.finalize_generation(self.actors)

    def run_sim(self, n_iter):
        """Run Simulation for n steps."""
        for gen in range(n_iter):
            _logger.info(f" ============ Simulating Generation {gen} ============")
            self._sim_step()
