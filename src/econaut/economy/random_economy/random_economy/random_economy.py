import random

from econaut.economy.abstract_economy import AbstractEconomy
from econaut.model.actor import Actor
from econaut.model.product import Product
from econaut.model.tech_forest import TechForest


class RandomEconomy(AbstractEconomy):
    """A very naive implementation of AbstractEconomy, mainly to test the sim loop."""

    def __init__(self, tech: TechForest):
        self._tech = tech

    def init_sim_step(self):
        pass

    def get_production_motivation(self, actor: Actor, prod: Product) -> float:
        return 0.5

    def distribute_production(self, producer: Actor, products: [Product]):
        """just lazily assign production to producer."""
        producer.receive_stuff(products)

    def exchange_goods(self, actors: [Actor]):
        """Our lazy agents don't exchange anything"""
        pass

    def update_actor_target_product(self, actor: Actor):
        if random.random() < 0.05:
            actor.target_product = self._tech.get_random_known_product()
