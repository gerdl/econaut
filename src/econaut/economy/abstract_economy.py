from abc import ABC, abstractmethod

from econaut.model.actor import Actor
from econaut.model.product import Product
from econaut.model.product_type import ProductType


class AbstractEconomy(ABC):
    pass

    @abstractmethod
    def init_sim_step(self):
        """Give Economy a chance to initialize this step"""
        raise NotImplementedError()

    @abstractmethod
    def get_production_motivation(self, actor: Actor, prod: Product) -> float:
        """Return a float between 0 and 1"""
        raise NotImplementedError()

    @abstractmethod
    def distribute_production(self, producer: Actor, products: [Product]):
        """
        Decide who gets the results of production efforts.

        It would be possible to store the production results in a buffer until
        exchange_goods() is called, for example if a village population would
        put all results of production onto a pile in the middle first...
        """
        raise NotImplementedError()

    @abstractmethod
    def exchange_goods(self, actors: [Actor]):
        pass

    @abstractmethod
    def update_actor_target_product(self, actor: Actor):
        pass
