import logging
import random

from econaut.config.logsetup import logsetup
from econaut.economy.random_economy.random_economy.random_economy import RandomEconomy
from econaut.model.tech_forest import TechForest
from econaut.sim.sim_stats import SimStats
from econaut.sim.simple_sim import SimpleSim

if __name__ == "__main__":
    logsetup()
    _logger = logging.getLogger(__name__)
    logging.getLogger("econaut.model.actor").setLevel(logging.WARNING)

    _logger.info("Starting setup of simulation:")
    tech_rng = random.Random(1234)
    tech = TechForest(tech_rng)
    stats = SimStats()
    economy = RandomEconomy(tech=tech)
    s1 = SimpleSim(economy=economy, stats=stats)

    _logger.info("Starting simulation run")
    s1.run_sim(50)

    _logger.info("Simulation done.")
