"""
Creates a random SatisfierVector for a ProductType or an AccumulatingNeedVector.

A SatisfierVector is a NeedVector that stores as a constant, how much a product
can satisfy an actor's needs upon use.

An accumulator NeedVector starts with zero and adds up, dependent on category
with each new consumed product.

"""
import random

from math import sqrt

N_DIMENSIONS = 5


class SatisfierVector:
    """
    The constant SatisfierVector captures how much of an actor's needs can be
    satisfied by using the product once.

    """
    def __init__(self, rng: random.Random):
        """
        Create a single random SatisfierVector instance

        We don't want a uniform distribution here: Each product should satisfy in
        1, 2 or 3 dimensions only with a total scaled to about 1.0, such that we
        don't get the "super-potato" that satisfies, food, drink, housing and heating
        all in a single byte of potato.

        """
        self.needs = [0.0 for _ in range(N_DIMENSIONS)]

        n_satisfyable_dims = rng.randint(1, 3)
        for _ in range(n_satisfyable_dims):
            # We don't externally distinguish between essential/non-essential goods
            #  any more. Being essential is implicitly defined by need-dimensions.

            dim = rng.randrange(0, N_DIMENSIONS)
            self.needs[dim] += rng.uniform(0.1, 1)

        # compute current length:
        sum_sq = 0.0
        for i in range(N_DIMENSIONS):
            sum_sq += self.needs[i]**2

        # rescale
        desirable_vector_length = rng.uniform(0.2, 1.5)
        vector_scale = desirable_vector_length / sqrt(sum_sq)
        for i in range(N_DIMENSIONS):
            self.needs[i] *= vector_scale


class AccumulatingNeedVector:
    """
    The immutable SatisfierVector captures how much of an actor's needs were already
    satisfied this iteration or how much needs a productType can satisfy.

    If no _new_need_vec is specified, a new "empty" need vector with all values set
    to zero is returned.
    """

    def __init__(self, _new_need_vec=None):
        if _new_need_vec is None:
            self._needs = [0.0 for _ in range(N_DIMENSIONS)]
        else:
            self._needs = _new_need_vec

    def add_satisfaction_from_product_use(self,
                                          satisfier: SatisfierVector,
                                          preference_factor: float,
                                          n_uses: int) -> 'AccumulatingNeedVector':

        new_needs = [self._needs[i] + satisfier.needs[i] * preference_factor * n_uses
                     for i in range(N_DIMENSIONS)]
        return AccumulatingNeedVector(_new_need_vec=new_needs)

    def effective_satisfaction_score(self) -> float:
        """
        Compute a scalar satisfaction score:

        The minimum value over all need categories.

        """
        return min(self._needs)
