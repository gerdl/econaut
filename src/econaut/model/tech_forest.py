import random

from econaut.model.product_type import ProductType


class TechForest:
    """
    What is usually known as a 'tech-tree' in computer games.

    A forest of trees of ProductTypes that describes what is required to produce this Product.

    If equipped with an independent random generator, we can have a deterministic, unlimited, procedurally generated
    tech forest here, that can always generate more elements if required.

    We call it 'forest', because it is a set of trees, not a single tree.
    A single tree would describe the production of a single productType.
    """

    def __init__(self, rng: random.Random):
        self.forest = list()
        self.known = list()
        self.novel = list()
        self._rng = rng

        for i in range(20):

            # simple products that don't require others to be produced
            new_prod = ProductType(self._rng)
            self.forest.append(new_prod)

        # let's assume the initial products are known:
        #   Not a pointer to the forest, but a copy!
        self.known = list(self.forest)

    def get_random_known_product(self):
        """
        Get a product that had been produced before and is therefore "known".

        We don't use self._rng here, as we only want to have a deterministic tech tree,
        not a deterministic access to the tech tree.
        """

        prod = self.known[random.randrange(len(self.known))]
        return prod

    def get_novel_product(self):
        """
        Get arbitrary product that can be produced but was not produced before.

        It is a novel product iff
        - All the product's requirements are known
        - The product was not produced before
        """
        raise NotImplementedError()
