from econaut.model.product_type import ProductType


class Product:
    def __init__(self, tp: ProductType, _quantity=1.0):
        self.ptype = tp

        # a floating point value to store how many products of this type our econaut owns or trades
        #  When having 3.123 "houses", the fourth house will be lived in, the other 3 are empty and
        #  are only in the possession of the econaut.
        self.quantity = _quantity

    def use(self, n_uses: int):
        """
        Use this product n times
        """
        if self.quantity < self.ptype.consumption_per_use * n_uses:
            raise ValueError(f"This product cannot be used {n_uses} times - we don't have enough.")
        self.quantity -= self.ptype.consumption_per_use * n_uses

    @property
    def is_used_up(self):
        return self.quantity < self.ptype.consumption_per_use

    @property
    def remaining_quality(self):
        return self.quantity
