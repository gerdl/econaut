import logging
import random

import math

from econaut.model.need_vector import AccumulatingNeedVector
from econaut.model.product import Product
from econaut.sim.sim_stats import SimStats
from econaut.utils import hashi


_global_actor_counter = 1000
_logger = logging.getLogger(__name__)


class Actor:
    def __init__(self):
        """Generate a randomly initialzed actor"""

        self.work_per_day = random.uniform(0.05, 2.0)
        self.talented = random.uniform(0.5, 1.5)
        self.education = random.uniform(0.1, 5.0)

        self.consumption_pref_hash = hashi.get_rand_hash()
        self.production_pref_hash = hashi.get_rand_hash()

        # what is accessible by the actor?
        #  dict from ProductType to Product
        self._possession = dict()

        # are the actor's needs satisfied after consumption?
        self._needs = AccumulatingNeedVector()

        # what is being produced and how far are we in the process?
        self.target_product = None
        self._production_process = 0.0

        global _global_actor_counter
        self.name = str(_global_actor_counter)
        _global_actor_counter += 1

    def produce(self, motivation):
        """
        Find out how far our production progresses today and generate the integer number produced.
        The remainder is kept self._production_process
        The produced products are NOT added to the possessions - the economic system needs
        to decide who gets the produced stuff!
        """

        # preference hashing:
        production_preference = hashi.to_float(
            self.production_pref_hash ^ self.target_product.pref_hash
        )

        progress = (
                self.work_per_day
                * self.talented
                * self.education
                * motivation
                * production_preference
        )

        total = self._production_process + progress
        produced = int(total)
        remainder = total - produced
        self._production_process = remainder

        if produced >= 1:
            _logger.debug(f"Actor {self.name} produced {produced} {self.target_product.name}.")

        return produced

    def consume_possessions(self, stats: SimStats) -> None:
        """
        Reduce product quantities until particular products are used up

        - We are greedy: If we can, we will consume it - limited by the product-specific
          may-use-per-day value!
        - We aren't picky: If we have it, we consume it - even if we don't like it much!
        - We are stupid: We don't start consuming products that we like;
          We start consuming whatever arbitrarily comes first!
        - For each possessed productType, we can use between
          0 and min(quantity/consumption_per_use,
                    max_use_per_day) times
        -

        """
        # Reset needs for this iteration:
        self._needs = AccumulatingNeedVector()

        # Note: Is it possible to consume multiple instances of a product?
        #   yes -> greedily consume at most ProductType.max_use_per_time

        used_up_container = set()

        for ptype, poss in self._possession.items():

            # skip non-consumer goods:
            if not ptype.is_consumer_good:
                continue

            # Use preference hashing to see how much this product suits the actor:
            consumption_preference = hashi.to_float(
                self.consumption_pref_hash ^ self.target_product.pref_hash
            )

            # Note: Later we might consider having an upper bound on the satisfaction
            #       in a particular category. For now we assume we can increase satisfaction
            #       unlimited in each category, therefore the number of uses that satisfies
            #       us is unlimited!

            # How much is left to consume
            n_consumptions_available = math.floor(poss.quantity / ptype.consumption_per_use)

            n_use = min(n_consumptions_available,
                        ptype.max_use_per_day,
                        )

            # consume n_use times!
            self._needs = self._needs.add_satisfaction_from_product_use(
                ptype.satisfaction,
                consumption_preference,
                n_use
            )

            poss.use(n_use)
            _logger.debug(f"Actor {self.name} used {ptype.name} - remaining {poss.remaining_quality}")
            if poss.is_used_up:
                used_up_container.add(ptype)
            stats.register_consumption(product_type=ptype,
                                       quantity=ptype.consumption_per_use * n_use,
                                       actor=self)

        # remove consumed products from possessions:
        for ptype in used_up_container:
            del self._possession[ptype]
            _logger.debug(f"Actor {self.name} used up all its {ptype.name}.")

    def spoil_possessions(self, stats: SimStats) -> None:
        """
        Reduce product quantities according to spoil-rate.

        How to implement spoiling?
        For now, let's just subtract a deterministic factor.

        """
        used_up = set()

        for ptype, poss in self._possession.items():

            spoilage = poss.quantity * ptype.spoil_rate
            poss.quantity -= spoilage
            if poss.quantity <= ptype.consumption_per_use:
                poss.quantity = 0.0

            if poss.is_used_up:
                used_up.add(ptype)

            stats.register_spoilage(product_type=ptype,
                                    quantity=spoilage,
                                    actor=self)

        # remove consumed products from possessions:
        for ptype in used_up:
            del self._possession[ptype]

    @property
    def possession_dict(self) -> dict:
        return self._possession

    @property
    def needs(self) -> AccumulatingNeedVector:
        return self._needs

    def receive_stuff(self, new_stuff: [Product]):
        """Add quantity of new_stuff products to our existing products or add new entry in possession dict."""
        for stuff in new_stuff:
            if stuff.ptype in self._possession:
                self._possession[stuff.ptype].quantity += stuff.quantity
            else:
                self._possession[stuff.ptype] = stuff
