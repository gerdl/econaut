import random
from econaut.config.econaut_config import conf, CF_HASH_BITS
from econaut.model.need_vector import SatisfierVector
from econaut.utils import hashi


_global_type_counter = 0


class ProductType:
    def __init__(self, rng: random.Random):
        """Simple Constructor to generate a random product."""
        self.consumption_per_use = rng.choice([1, 1, 1, 1, 0.1, 0.1, 0.01, 0.001])
        self.max_use_per_day = rng.choice([1, 1, 10, 10, 10, 100, 100])
        self.prod_manpower = rng.uniform(0.01, 15.0)

        # only consumer goods can be essential
        self.is_consumer_good = rng.choice([True, False, False])

        # spoil_rate: 0.1 means 10 percent per simulation day spoils
        self.spoil_rate = rng.choice([1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.01, 0.001])

        self.pref_hash = hashi.get_rand_hash()

        # How much satisfaction can an actor obtain from this product type?
        if self.is_consumer_good:
            self.satisfaction = SatisfierVector(rng)
        else:
            self.satisfaction = None

        global _global_type_counter
        self.name = "Pr" + str(_global_type_counter)
        _global_type_counter += 1

        # TODO: self.attractivity; A general attractivity, independent of the observer -
        #   e.g., a top vs a flop product
        #   Could also be completely relative in category - e.g., a newer generation cell phone
        #   is always nicer than the last generation...

        # TODO: to be added later:
        # - requirements
        # - prod_discount
        # - replaces_list
        # - is_industrial
        # - product_quality (might be included in the attractivity)
