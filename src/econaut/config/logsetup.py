import logging


def logsetup():
    handler = logging.StreamHandler()
    handler.setFormatter(
        logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    )

    rootlogger = logging.getLogger()
    rootlogger.addHandler(handler)
    rootlogger.setLevel(logging.DEBUG)
