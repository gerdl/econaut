import os
from setuptools import setup


# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


with open('requirements.txt') as fp:
    install_requires = fp.read()

setup(
    name="econaut",
    version="0.0.1snap",  # TODO: read version from git tag
    author="Gerd Gruenert",
    author_email="gerd.gruenert@posteo.de",
    description=("A simulation tool for exploring different economic systems."),
    license="GPLv2",
    keywords="economy simulation",
    packages=['econaut'],
    long_description=read('../Readme.md'),
    install_requires=install_requires,
)


